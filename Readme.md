# PostgreSQL master-slave cluster

## Prerequisites
- Vagrant - used for VM orchestration
- Ansible - used to configure the VMs
- Virtualbox - used as the VM backend

There is a Vagrantfile that holds all the information for the VMS and uses ansible to provision a 2-node primary-replica PostgrSQL cluster.

## Spin up the VMs
```
vagrant up
```

## Check replication status
After the VMs have been provisioned the cluster should be up and running. To check the replication status:

1. connect to the primary node
```
vagrant ssh vm1
```

2. inside the VM run the following command to check replication status:
```
sudo -u postgres psql -c "SELECT client_addr, state FROM pg_stat_replication;"
```

The expected output is the following:
```
   client_addr   |   state
-----------------+-----------
 192.168.100.102 | streaming
(1 row)
```

## Known issues/limitations
- A secure store for credentials should be used in a real world scenario (at least ansible vault for the passwords).
- Changing N in Vagrantfile to anything else than 2 wont work, because the replication policy only adds a single IP.
- In a production environment we should not restart the database if there were no changes in the db configuration and we should avoid just deleting the datafiles in the replica if replication is already setup.
